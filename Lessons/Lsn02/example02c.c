/** example02c.c
* ===========================================================
* Name: Troy Weingart, 4 Jan 18
* Section: n/a
* Project: Lesson 2 examples
* Purpose: Demonstrate basic c
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAXSTUDENTS 100

// define our cadet info type for later use
typedef struct {
    char name[50];
    int age;
    int squad;
    int classYear;
} MyCadetInfo;

// Prototypes for functions to be used in this file,
// remember all things in c must be declared before use
// which is why the typedef is before its use in the
// prototypes below
void printCadetInfo(MyCadetInfo cadetRecord);
int getData(MyCadetInfo datums[]);
void printData(MyCadetInfo* datums, int numCadets);
MyCadetInfo oldestCadet(MyCadetInfo datums[], int numCadets);

int main() {
    // declare an array of cadet records
    MyCadetInfo cadetRecords[MAXSTUDENTS];

    // read in the cadet info from a file
    int numCadets = getData(cadetRecords);

    // print all cadet records
    printData(cadetRecords, numCadets);

    // print the info related to the oldest cadet
    printCadetInfo(oldestCadet(cadetRecords,numCadets));

    return 0;
}

/** ----------------------------------------------------------
 * oldestCadet() finds the oldest cadet (ties go
 * to first cadet in the list)
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return a record of oldest cadet
 */
MyCadetInfo oldestCadet(MyCadetInfo datums[], int numCadets) {
    int oldest = 0;
    MyCadetInfo tempCadet;
    for (int i = 0; i < numCadets; i++) {
        if (datums[i].age > oldest) {
            tempCadet = datums[i];
            oldest = datums[i].age;
        }
    }
    return tempCadet;
}

/** ----------------------------------------------------------
 * printCadetInfo() is used to print a MyCadetInfo typed variable
 * to the console
 * @param cadetRecord is the cadet info struct to be printed
 * @return n/a
 */
void printCadetInfo(MyCadetInfo cadetRecord) {
    printf("Cadet name = \t%s\n", cadetRecord.name);
    printf("Cadet age = \t%d\n", cadetRecord.age);
    printf("Cadet squad = \t%d\n", cadetRecord.squad);
    printf("Cadet year = \t%d\n\n", cadetRecord.classYear);
}


/** ----------------------------------------------------------
 * getData() is used to read MyCadetInfo typed records
 * from a file
 * @param list is the array of cadet records
 * @return number of records read
 */
int getData(MyCadetInfo datums[]) {

    // Open an input file for reading
    FILE *in = fopen("../Lessons/Lsn02/lsn02Data.txt", "r");
    if (in == NULL) {
        printf("Error opening file: %s.\n", strerror(errno));
        exit(1);
    }

    char firstName[30];
    char lastName[45];
    int numRead = 0;
    while (numRead < MAXSTUDENTS && !feof(in)) {
        fscanf(in,"%s %s %d %d %d", firstName, lastName, &datums[numRead].age, &datums[numRead].squad, &datums[numRead].classYear);
        strcat(firstName," ");
        strcpy(datums[numRead].name, strcat(firstName,lastName));
        numRead++;
    }

    return numRead;
}

/** ----------------------------------------------------------
 * printData() is used to print MyCadetInfo typed records
 * from a file
 * @param datums is the array of cadet records
 * @param numCadets is the number of cadets in datums
 * @return n/a
 */
void printData(MyCadetInfo* datums, int numCadets) {
    for (int i = 0; i < numCadets; i++) {
        printCadetInfo(datums[i]);
    }
}

