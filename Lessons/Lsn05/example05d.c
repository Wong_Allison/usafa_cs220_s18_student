/** example05c.c
 * ===========================================================
 * Name: Troy Weingart
 * Section: n/a
 * Project: Lesson 5 Big-O example
 * ===========================================================
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Example #1 find f(n) & Big-O
    int n = atoi(argv[1]);
    int nums[n];

    //fill the array with random values
//    long count = 0;
    for (int i = 0; i < n; i++) {
        nums[i] = rand() % 10000;
    }

    // sort the array
    int largest_index = 0;
    int largest = 0;
    for (int i = 0; i < n; i++) {
        largest_index = 0;
        largest = 0;
        for (int j = i; j < n; j++) {
            if (nums[j] >= largest) {
                largest = nums[j];
                largest_index = j;
            }
//            count += 4;
        }
        nums[largest_index] = nums[i];
        nums[i] = largest;
//        count += 5;
    }
//    printf("%d\t%ld\n", n, count);
    return 0;
}