/** example03a.h
 * ===========================================================
 * Name: Troy Weingart, 4 Jan 18
 * Section: n/a
 * Project: Lesson 3 examples
 * Purpose: Demonstrate headers and recursion in C
 * ===========================================================
 */
#ifndef USAFA_CS220_S18_STUDENT_NONRECURSIVE_H
#define USAFA_CS220_S18_STUDENT_NONRECURSIVE_H

/* ----------------------------------------------------------
 * sumOfInts() sumOfIntsRec() calcuate the sum of integers
 * from 1 to the number passed into the function
 * @param num is the upper bound on the sub
 * @return sum
 * ----------------------------------------------------------
 */
int sumOfInts(int num);
int sumOfIntsRec(int num);

/* ----------------------------------------------------------
 * factorial() computes the factorial of passed number
 * @param num is the operand of the factorial operation
 * @return num!
 * ----------------------------------------------------------
 */
int factorial(int num);
int factorialRec(int num);

/** ----------------------------------------------------------
 * numberOfRec() counts the number of time a given number
 * appears in an array of integers
 * @param array is the array of integers
 * @param index is the starting index (typically the end)
 * @param target is the integer to be counted
 * @return the number of target integers in the array
 * ----------------------------------------------------------
 */
int numberOfRec(int array[], int index, int target);

#endif //USAFA_CS220_S18_STUDENT_NONRECURSIVE_H
