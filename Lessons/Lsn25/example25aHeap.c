/** example25aHeap.c
 * ===========================================================
 * Name: Dr. Troy Weingart
 * Section:
 * Purpose: Implement a heap ADT and heap sort
 * ===========================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "example25aHeap.h"

void heapifyUp(Heap* heap) {

    int childIndex = heap->numElem;
    int parentIndex = childIndex / 2;
    int key = heap->array[childIndex];

    while (parentIndex > 0) {
        // key is smaller than parent so we are done
        if (key < heap->array[parentIndex]) {
            break;
        }

        // key is larger than parent; so move up tree
        heap->array[childIndex] = heap->array[parentIndex];

        // move up the tree
        childIndex = parentIndex;
        parentIndex = childIndex / 2;
    }
    // place key value in proper location
    heap->array[childIndex] = key;

}

void heapifyDown(Heap* heap) {
    int rootIndex = 1;
    int key = heap->array[rootIndex];
    int childIndex = 2 * rootIndex;

    while (childIndex <= heap->numElem) {
        // Is there a right child
        if (childIndex < heap->numElem) {
            // Find larger child
            if (heap->array[childIndex] < heap->array[childIndex+1]) {
                childIndex++;
            }
        }
        // check to see if key is larger than both children
        if (key > heap->array[childIndex]) {
            break;
        }

        // key is smaller; promote child to parent
        heap->array[rootIndex] = heap->array[childIndex];

        // Move down tree
        rootIndex = childIndex;
        childIndex = 2 * rootIndex;
    }
    // place key in proper place
    heap->array[rootIndex] = key;
}

Heap* maxHeapCreate() {
    Heap* heap;
    heap = malloc(sizeof(Heap));
    heap->numElem = 0;
    return heap;
}

void insertElemHeap(Heap* heap, int element) {
    // Make sure the array has room for the new element
    if (heap->numElem >= MAX_HEAP_SIZE ) {
        printf("Error in insert, the array is too small.\n");
        exit(1);
    }
    // Add the element to the end of the array
    heap->array[heap->numElem+1] = element;

    // Make the heap size one more
    heap->numElem++;

    // heapify ensuring new value is in its proper place in the heap
    heapifyUp(heap);

}

int removeElemHeap(Heap* heap) {
    // Make sure the array isn't empty
    if (heap->numElem > 0 ) {
        printf("Error in remove, the array is empty.\n");
        exit(1);
    }

    // stop the value at the top of the heap for return later
    int top = heap->array[1];

    // copy over the top of the heap with the last leaf node
    // and then heapify moving that element down the heap
    heap->array[1] = heap->array[heap->numElem];
    heap->numElem--;
    heapifyDown(heap);

    // return the top element in the heap
    return top;
}

void sortHeap(Heap* heap) {

    while (heap->numElem > 0) {
        int temp = heap->array[1];
        heap->array[1] = heap->array[heap->numElem];
        heap->array[heap->numElem] = temp;
        heap->numElem--;
        heapifyDown(heap);
    }
}

void displayHeap(Heap* heap) {
    for (int i = 1; i < MAX_HEAP_SIZE; i++) {
        printf("%d\n",heap->array[i]);
    }
    printf("\n");
}