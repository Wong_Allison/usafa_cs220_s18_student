/** wordList.h
 * ===========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 */

#ifndef PEX2_TST_WORDLIST_H
#define PEX2_TST_WORDLIST_H


// ===================================================================================================================
// Constants/Macros
// ===================================================================================================================

#define MAXSTRINGLENGTH 20

// ===================================================================================================================
// Type declarations
// ===================================================================================================================

typedef struct wordList {
    int numElements;
    char **words;
} WordList;

/** ----------------------------------------------------------
 * loadDictionary() loads words from a text file into a WordList
 * @param dictionary - a pointer to a WordList in which the words are loaded
 * @param file - the file where the words are located
 * ----------------------------------------------------------
 */
void loadDictionary(WordList* dictionary, char* file);

/** ----------------------------------------------------------
 * printDictionary() prints the words in order
 * @param dictionary - a pointer to the WordList of words to print
 * ----------------------------------------------------------
 */
void printWordList(WordList* dictionary);

// ===================================================================================================================
// Constructors/Destructors
// ===================================================================================================================
/** ----------------------------------------------------------
 * wordListCreate() - creates a new WordList
 * @return the initialized WordList
 * ----------------------------------------------------------
 */
WordList * wordListCreate();

/** ----------------------------------------------------------
 * wordListDelete() - frees the memory used by a WordList data structure
 * @param words - a pointer to the WordList to delete
 * ----------------------------------------------------------
 */
void wordListDelete(WordList *words);


// ===================================================================================================================
// Modifier functions
// ===================================================================================================================

/** ----------------------------------------------------------
 * wordListAdd() - adds a word to the end of a WordList
 * @param words - an initialized WordList
 * @param word - the word to add to the list
 * @return true if the word was added, false otherwise.
 * ----------------------------------------------------------
 */
int wordListAdd(WordList *words, char *word);

/** ----------------------------------------------------------
 * wordListCombine() - combines two WordLists together by copying the source WordList into the destination WordList.
 *      Note:  The combined WordList will be in sorted order.
 * @param dest - a WordList pointer
 * @param source - a WordList pointer
 * @return the combined Wordlist
 * ----------------------------------------------------------
 */
WordList * wordListCombine(WordList *dest, WordList *source);

/** ----------------------------------------------------------
 * wordListAppendPrefix - appends a prefix to every word in the WordList
 * @param words - an initialized WordList
 * ----------------------------------------------------------
 */
void wordListAppendPrefix(WordList *words, char *prefix);

// ===================================================================================================================
// Utility functions
// ===================================================================================================================

/** ----------------------------------------------------------
 * wordListGet - returns the word at the index value.
 * @param words - a WordList pointer
 * @param index - the index value of the word to retrieve
 * @return the word that was retrieved, or NULL if no such index exists
 * ----------------------------------------------------------
 */
char * wordListGet(WordList *words, int index);

/** ----------------------------------------------------------
 * wordListSize - return the number of words in the WordList
 * @param words - a WordList pointer
 * @return the number of words in the WordList, or 0 if words is not initialized
 * ----------------------------------------------------------
 */
int wordListSize(WordList *words);


void stripNewLine(char *word);


#endif //PEX2_TST_WORDLIST_H
