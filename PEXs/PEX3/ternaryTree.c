/** ternaryTree.c
 * ===========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ternaryTree.h"

/** ----------------------------------------------------------
 * ternaryTreeCreate() creates a ternary tree, by creating a root
 * @return the ternary tree
 * ----------------------------------------------------------
 */
TernaryTreeType* ternaryTreeCreate(){
    TernaryTreeType* tree = malloc(sizeof(TernaryTreeType));
    tree->root = malloc(sizeof(NodeType));
    return tree;
}

/** ----------------------------------------------------------
 * createNode() creates a new node, given a letter
 * @param - the char to declare as the element of the new node
 * @return the new node
 * ----------------------------------------------------------
 */
NodeType* createNode(char letter){
    NodeType* node = malloc(sizeof(NodeType));
    node->element = letter;
    node->left = NULL;
    node->middle = NULL;
    node->right = NULL;
    return node;
}

/** ----------------------------------------------------------
 * ternaryTreeDelete() deletes the ternary tree
 * @param tree - a pointer to the root of the ternary tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeDelete(TernaryTreeType* tree){
    if(tree->root == NULL){
        printf("Tree already deleted.");
    }
    if(tree->root != NULL){
        ternaryTreeDeleteRec(tree->root);
    }
    free(tree->root);
}

void ternaryTreeDeleteRec(NodeType* nodePtr){
    if(nodePtr != NULL){
        ternaryTreeDeleteRec(nodePtr->left);
        ternaryTreeDeleteRec(nodePtr->middle);
        ternaryTreeDeleteRec(nodePtr->right);
        free(nodePtr);
    }
}

/** ----------------------------------------------------------
 * ternaryTreeInsertBalanced() inserts words into the ternary tree
 * @param tree - a pointer to the root of the tree
 * @param words - the list of words to insert into the tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeInsertBalanced(WordList* words, TernaryTreeType* tree){
    int start = 0;
    int end = wordListSize(words);
    ternaryTreeInsertBalancedRec(words, tree, start, end);
}

void ternaryTreeInsertBalancedRec(WordList* words, TernaryTreeType* tree, int lBound, int rBound){
    if(lBound < rBound){
        int mid = ((rBound-lBound)/2) + lBound;
        printf("\nInserting %s... ", wordListGet(words, mid));
        fflush(stdout);
        ternaryTreeInsertWord(tree, wordListGet(words, mid));
        ternaryTreeInsertBalancedRec(words, tree, lBound, mid);
        ternaryTreeInsertBalancedRec(words, tree, mid+1, rBound);
    }
}

/** ----------------------------------------------------------
 * ternaryTreeInsertWord() inserts the letters of a word into the ternary tree
 * @param tree - a pointer to the root of the tree
 * @param word - the word to insert into the tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeInsertWord(TernaryTreeType* tree, char* word){
    //Basically if tree doesn't exist at all yet
    if(tree->root == NULL) {
        tree->root = createNode(*word);
        printf("%c ", *word);
        fflush(stdout);
        ternaryTreeInsertWordRec(tree->root->middle, word + 1);
    }
    //Else, pass the word right down the existing tree
    else {
    printf("%c ", *word);
    fflush(stdout);
    ternaryTreeInsertWordRec(tree->root, word);
    }
}

void ternaryTreeInsertWordRec(NodeType* currentNode, char* word){
    //Reached end of the word
    if(*word == '\0'){
        printf("%c ", *word);
        fflush(stdout);
        currentNode->middle = createNode('\0');
        return;
    }
    else{
        if(currentNode->middle == NULL) {
            currentNode->middle = createNode(*word);
            printf("%c ", *word);
            fflush(stdout);
            ternaryTreeInsertWordRec(currentNode->middle, word + 1);
        }
        //If node matches word, keep recursing going down the middle of the tree
        else if(currentNode->element == *word) {
            printf("%c ", *word);
            fflush(stdout);
            ternaryTreeInsertWordRec(currentNode->middle, word + 1);
        }
        else if(*word < currentNode->element){ //If word is < the node, recurse & move to the left
            currentNode->left = createNode(*word);
            ternaryTreeInsertWordRec(currentNode->left, word+1);
        }
        else if(*word > currentNode->element){ //If word is > the node, recurse & move to the right
            currentNode->right = createNode(*word);
            ternaryTreeInsertWordRec(currentNode->right, word+1);
        }
    }
}

/** ----------------------------------------------------------
 * ternarytreeFindPattern() finds where a given pattern occurs in the tree
 * @param tree - a pointer to the root of the tree
 * @param pattern - the pointer to the pattern char
 * @return returns a pointer to the WordList structure containing the pattern
 * ----------------------------------------------------------
 */
WordList* ternaryTreeFindPattern(TernaryTreeType* tree, char* pattern){
    WordList* matches = wordListCreate();
    char* tempWord = NULL; //To store letters found, as you go
    ternaryTreeFindPatternRec(tree->root, pattern, tempWord);

    return matches;
}

WordList* ternaryTreeFindPatternRec(NodeType* nodePtr, char* pattern, char* tempWord){
    if(nodePtr->element == '\0' && tempWord == pattern){
        printf("Match found.");
        fflush(stdout);
        WordList* tempList = wordListCreate();
        wordListAdd(tempList, tempWord);
        return tempList;
    }
    else if(nodePtr->element == *pattern){
        tempWord = strcat(tempWord, pattern);
        printf("%s", tempWord);
        fflush(stdout);
        ternaryTreeFindPatternRec(nodePtr->middle, pattern+1, tempWord);
    }
    else if(nodePtr->element != *pattern){
        if((nodePtr->left == NULL && *pattern < nodePtr->element) || (nodePtr->right == NULL && *pattern > nodePtr->element)||
                (nodePtr->left == NULL && nodePtr->right == NULL)){
            printf("No match.");
            WordList* emptyList = wordListCreate();
            return emptyList;
        }
        else if(*pattern < nodePtr->element){
            ternaryTreeFindPatternRec(nodePtr->left, pattern, tempWord);
        }
        else if(*pattern > nodePtr->element){
            ternaryTreeFindPatternRec(nodePtr->right, pattern, tempWord);
        }
    }
}