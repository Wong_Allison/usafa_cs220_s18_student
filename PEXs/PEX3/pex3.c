/** pex3.c
 * ===========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * Documentation: Got help from Col Caswell.
 *                Asked C3C Landwehr questions concerning the command line.
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "wordList.h"
#include "ternaryTree.h"

//int main(int argc, char* argv[]){
//
//    WordList* dictionary = wordListCreate();
//    char* file = argv[0];
//    loadDictionary(dictionary, file);
//    printWordList(dictionary);
//    TernaryTreeType* tree = ternaryTreeCreate();
//    ternaryTreeInsertBalanced(dictionary, tree);
//
//    return 0;
//}


void loadDictionary(WordList* dictionary, char* filename){
    FILE *file = fopen(filename, "r");
    if(file == NULL){
        printf("Error: %s.\n", strerror(errno));
        exit(1);
    }
    char* tempWord = malloc(sizeof(MAXSTRINGLENGTH));
    while(!feof(file)){
        fscanf(file, "%s", tempWord);
        stripNewLine(tempWord);
        strcat(tempWord, "\0");
        wordListAdd(dictionary, tempWord);
    }
    fclose(file);
}

void printWordList(WordList* dictionary){
    int num = wordListSize(dictionary);
    for(int i = 0; i < num; i++) {
        char *word = wordListGet(dictionary, i);
        printf("%s \n", word);
        fflush(stdout);
    }
}


void stripNewLine(char *word){
    char *newLine;
    if((newLine = strchr(word, '\n')) != NULL){
        *newLine = '\0';
    }
    if ((newLine = strchr(word, '\r')) != NULL){
        *newLine = '\0';
    }
}