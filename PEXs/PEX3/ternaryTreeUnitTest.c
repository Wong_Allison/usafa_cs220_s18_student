/** ternaryTreeUnitTest.c
 * ===========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 */
#include <stdio.h>
#include "wordList.h"
#include "ternaryTree.h"


int main(int argc, char* argv[]){

    printf("-----LOAD AND PRINT DICTIONARY----- \n");
    fflush(stdout);
    WordList* dictionary = wordListCreate();
    char* filename = "../PEXs/PEX3/test_dict1.txt";
    loadDictionary(dictionary, filename);
    printWordList(dictionary); //Expected output: CAB CAD CAR CAT COP COT COW CUT

    printf("-----BALANCED & INSERTED TREE-----");
    fflush(stdout);
    TernaryTreeType* tree = ternaryTreeCreate();
    ternaryTreeInsertBalanced(dictionary, tree); //Expected output: Inserting COP... C O P
                                                                  //Inserting CAR... C A R
                                                                  //Inserting CAD... C A D
                                                                  //Inserting CAB... C A B
                                                                  //Inserting CAT... C A T
                                                                  //Inserting COW... C O W
                                                                  //Inserting COT... C O T
                                                                  //Inserting CUT... C U T

    printf("\n------FIND PATTERN IN TREE------ \n");
    fflush(stdout);
//    ternaryTreeFindPattern(tree, "CAT"); //Doesn't really work...

    return 0;
}