/** ternaryTree.h
 * ===========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX3
 * Purpose: Create and manipulate a list of words.
 * ===========================================================
 */
#ifndef USAFA_CS220_S18_STUDENT_TERNARYTREE_H
#define USAFA_CS220_S18_STUDENT_TERNARYTREE_H
#include "wordList.h"

typedef struct nodeType {
    char element;
    struct nodeType* left;
    struct nodeType* middle;
    struct nodeType* right;
} NodeType;

typedef struct ternaryTreeType {
    NodeType* root;
} TernaryTreeType;


/** ----------------------------------------------------------
 * ternaryTreeCreate() creates a ternary tree, by creating a root
 * @return the ternary tree
 * ----------------------------------------------------------
 */
TernaryTreeType* ternaryTreeCreate();


/** ----------------------------------------------------------
 * createNode() creates a new node, given a letter
 * @param - the char to declare as the element of the new node
 * @return the new node
 * ----------------------------------------------------------
 */
NodeType* createNode(char letter);


/** ----------------------------------------------------------
 * ternaryTreeDelete() deletes the ternary tree
 * @param tree - a pointer to the root of the ternary tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeDelete(TernaryTreeType* tree);
void ternaryTreeDeleteRec(NodeType* nodePtr);

/** ----------------------------------------------------------
 * ternaryTreeInsertBalanced() inserts words into the ternary tree
 * @param tree - a pointer to the root of the tree
 * @param words - the list of words to insert into the tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeInsertBalanced(WordList* words, TernaryTreeType* tree);
void ternaryTreeInsertBalancedRec(WordList* words, TernaryTreeType* tree, int lBound, int rBound);

/** ----------------------------------------------------------
 * ternaryTreeInsertWord() inserts the letters of a word into the ternary tree
 * @param tree - a pointer to the root of the tree
 * @param word - the word to insert into the tree
 * @return none
 * ----------------------------------------------------------
 */
void ternaryTreeInsertWord(TernaryTreeType* tree, char* word);
void ternaryTreeInsertWordRec(NodeType* root, char* word);

/** ----------------------------------------------------------
 * ternarytreeFindPattern() finds where a given pattern occurs in the tree
 * @param tree - a pointer to the root of the tree
 * @param pattern - the pointer to the pattern char
 * @return returns a pointer to the WordList structure containing the pattern
 * ----------------------------------------------------------
 */
WordList* ternaryTreeFindPattern(TernaryTreeType* tree, char* pattern); //returns list of words found through next func
WordList* ternaryTreeFindPatternRec(NodeType* nodePtr, char* pattern, char* tempWord); //returns word found in that recursion

#endif //USAFA_CS220_S18_STUDENT_TERNARYTREE_H
