/** listAsLinkedList.h
* ================================================================
* Name: first last
* Section: xxx
* Project: Header for Linked List Library used in PEX2
* =================================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Define the type for a segment of the snake
typedef struct {
    int x;  // the x location of the segment
    int y;  // the y location of the segment
} Segment;

// Define type for a node of the linked list
typedef struct node {
    Segment segment;  // our list now contains a segment
    struct node* next;
} Node;

// Define the type (meta-segment) that manages the linked list of nodes
typedef struct {
    Node* head;
    Node* tail;
    int numberOfElements;
} LinkedList;

/** ----------------------------------------------------------
 * createLinkedList() allocates and initializes the linked list
 * meta segment
 * @return returns a pointer to the allocated linked list meta
 * segment
 * ----------------------------------------------------------
 */
LinkedList* createLinkedList();
LinkedList* createLinkedList(){
    LinkedList* list = malloc(sizeof(LinkedList));
    list->head = NULL;
    list->tail = NULL;
    list->numberOfElements = 0;
    return list;
}


/** ----------------------------------------------------------
 * deleteLinkedList() frees all memory associated with the linked
 * list to include the meta segment and all the nodes in the list
 * @param list - a pointer to the linked list
 * ----------------------------------------------------------
 */
void deleteLinkedList(LinkedList* list);
void deleteLinkedList(LinkedList* list) {
    Node *tempPtr = list->head;
    Node *freePtr = list->head;
    while (freePtr != NULL) {
        tempPtr = tempPtr->next;
        freePtr = tempPtr;
    }
    list->head = NULL;
    list->tail = NULL;
    list->numberOfElements = 0;
}

/** ----------------------------------------------------------
 * appendElementLinkedList() appends an element to the list
 * @param list - pointer to the linked list
 * @param element - the element to append
 * ----------------------------------------------------------
 */
void appendElementLinkedList(LinkedList* list, Segment element);
void appendElementLinkedList(LinkedList* list, Segment element) {
    Node *tempNode = malloc(sizeof(Node));
    tempNode->segment = element;
    tempNode->next = NULL;
    if (list->head != NULL) {
        list->tail->next = tempNode;
        list->tail = tempNode;
        list->numberOfElements++;
    }
    else{
        list->head = tempNode;
        list->tail = tempNode;
        list->numberOfElements++;
    }
}

/** ----------------------------------------------------------
 * lengthOfLinkedList() returns the length of the linked list
 * @param pointer to the linked list
 * @return the length of the linked list of nodes
 * ----------------------------------------------------------
 */
int lengthOfLinkedList(LinkedList* list);
int lengthOfLinkedList(LinkedList* list){
    int count = 0;
    Node* tempList = list->head;
    while(tempList != NULL){
        count++;
        tempList = tempList->next;
    }
    return count;
}

/** ----------------------------------------------------------
 * printLinkedList() prints the linked list of nodes
 * @param list - a pointer to the linked list
 * ----------------------------------------------------------
 */
void printLinkedList(LinkedList* list);
void printLinkedList(LinkedList* list){
    if(list->head != NULL){
        Node* tempPtr = list->head;
        while(tempPtr != NULL) {
            printf("%d, %d\n", tempPtr->segment.x, tempPtr->segment.y);
            tempPtr = tempPtr->next;
        }
        printf("NULL\n");
    }
    else{
        printf("NULL\n");
    }

}

/** ----------------------------------------------------------
 * getElementLinkedList() gets an element from the linked list
 * it doesn't remove the element just returns it
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @return the requested element of the list
 * ----------------------------------------------------------
 */
Segment getElementLinkedList(LinkedList* list, int position);
Segment getElementLinkedList(LinkedList* list, int position){
    if(list->numberOfElements == 0){
        printf("Error yee");
        exit(1);
    }
    Node* tempPtr = list->head;
    for(int i = 0; i < position; ++i) {
        tempPtr = tempPtr->next;
    }
    return tempPtr->segment;
}


/** ----------------------------------------------------------
 * changeElementLinkedList() changes an element in the linked list
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @param newElement - the new segment to place into the list over writing
 * the element in the list at position
 * ----------------------------------------------------------
 */
void changeElementLinkedList(LinkedList* list, int position, Segment newElement);
void changeElementLinkedList(LinkedList* list, int position, Segment newElement){
    if(list->numberOfElements == 0){
        printf("Error");
        exit(1);
    }
    int i = 0;
    Node* overwrite = list->head;
    while(i < position){
        i++;
        overwrite = overwrite->next;
    }
    overwrite->segment = newElement;
}

/** ----------------------------------------------------------
 * deleteElementLinkedList() deletes an element from the linked list
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * ----------------------------------------------------------
 */
void deleteElementLinkedList(LinkedList* list, int position);
void deleteElementLinkedList(LinkedList* list, int position){
    if(position >= list->numberOfElements){
        printf("Error");
        exit(1);
    }
    else if(position == 0){
        list->head = list->head->next;
    }
    else if(position == list->numberOfElements-1){
        Node* tempNode = list->head;

        int i = 0;
        while(i < list->numberOfElements-2){
            tempNode = tempNode->next;
            i++;
        }
        list->tail = tempNode;
        list->tail->next = NULL;
    }
    else{
        Node* tempNode = list->head;
        int i = 0;
        while(i < position-1) {
            tempNode = tempNode->next;
            i++;
        }
        tempNode->next = tempNode->next->next;
    }
}


/** ----------------------------------------------------------
 * insertElementLinkedList() inserts an element into the linked list
 * the inserted element is inserted at the position given (if you
 * insert at position n the new element is at position n after the
 * insert)
 * @param list - a pointer to the linked list
 * @param position - the position in the list 0..length-1
 * @param element - the element to insert into the list
 * ----------------------------------------------------------
 */
void insertElementLinkedList(LinkedList* list, int position, Segment element);
void insertElementLinkedList(LinkedList* list, int position, Segment element){
    if(position > list->numberOfElements){
        return;
    }
    //Create new node
    Node* newNode = malloc(sizeof(Node));
    newNode->segment = element;
    newNode->next = NULL;
    //Insert node based on cases
    if(list->numberOfElements == 0){
        list->head = newNode;
        list->tail = list->head;
        list->numberOfElements++;
    }
    else if(position == 0){
        newNode->next = list->head;
        list->head = newNode;
        list->numberOfElements++;
    }
    else if(position == list->numberOfElements+1){
        appendElementLinkedList(list, element);
    }
    else{
        Node* tempPtr = list->head;
        int i = 0;
        while(i<position-1){
            i++;
            tempPtr = tempPtr->next;
        }
        newNode->next = tempPtr->next;
        tempPtr->next = newNode;
        list->numberOfElements++;
    }
}


/** ----------------------------------------------------------
 * findElementLinkedList() finds the position of a given element
 * in the linked list
 * @param list - a pointer to the linked list
 * @param element - the element to find in the linked list
 * @return position of the element or -1 if not found
 * ----------------------------------------------------------
 */
int findElementLinkedList(LinkedList* list, Segment element);
int findElementLinkedList(LinkedList* list, Segment element){
    int i = 0;
    Node* tempNode = list->head;

    while(i < list->numberOfElements){
        if(tempNode->segment.x == element.x && tempNode->segment.y == element.y){
            return i;
        }
        tempNode = tempNode->next;
        i++;
    }
    return -1;
}

#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
