/** pex2.c
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: PEX2 - Snake
 * Documentation Statement: C3C Dang helped me figure out how to end the game.
* ===========================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include "listAsLinkedList.h"
#include "pex2.h"

#define TICKRATE 100

#define WORLD_WIDTH 50
#define WORLD_HEIGHT 20

#define SNAKEY_LENGTH 1

enum direction { UP, DOWN, RIGHT, LEFT };

int main(int argc, LinkedList* snake){
    WINDOW *snakeys_world;
    int offsetx, offsety, i, ch;
    int score = 1;

    initscr();
    noecho();
    cbreak();
    timeout(TICKRATE);
    keypad(stdscr, TRUE);
    curs_set(0);

    printw("Snake - Press x to quit...\r");
    printw("Score: %d \r", score);

    refresh();

    offsetx = (COLS - WORLD_WIDTH) / 2;
    offsety = (LINES - WORLD_HEIGHT) / 2;

    snakeys_world = newwin(WORLD_HEIGHT,
                           WORLD_WIDTH,
                           offsety,
                           offsetx);

    LinkedList* snakey = createLinkedList();
    snakey->numberOfElements = SNAKEY_LENGTH;

    int sbegx = (WORLD_WIDTH - SNAKEY_LENGTH) / 2;

    int sbegy = (WORLD_HEIGHT - 1) / 2;

    for(i = 0; i < SNAKEY_LENGTH; i++){
        Segment tempSeg = {sbegx+i, sbegy};
        appendElementLinkedList(snakey, tempSeg);
    }

    int cur_dir = RIGHT;

    while ((ch = getch()) != 'x') {
        wclear(snakeys_world);

        Segment food = generate_food(snakeys_world);
        if (move_snakey(snakeys_world, cur_dir, snakey) == 1) {
            //End Game
            wclear(snakeys_world);
            while (getch() != 'x') {
                wclear(snakeys_world);
                printw("Game over. X to quit. Final score: %d .               \r ", snakey->numberOfElements - 1);
                wrefresh(snakeys_world);
                break;
            }
        }
        if (ch != ERR) {
            switch (ch) {
                case KEY_UP:
                    cur_dir = UP;
                    break;
                case KEY_DOWN:
                    cur_dir = DOWN;
                    break;
                case KEY_RIGHT:
                    cur_dir = RIGHT;
                    break;
                case KEY_LEFT:
                    cur_dir = LEFT;
                    break;
                default:
                    break;
            }
        }
        wrefresh(snakeys_world);
    }

    delwin(snakeys_world);
    endwin();
    return 0;
}

/**
 * move_snakey() - moves the snake around the screen
 * @param win - the WINDOW in which the snake moves
 * @param direction - the direction that the snake moves
 * @param snakey - the snake list
 * @return 1 if it's time for the game to end, 0 else
 */
int move_snakey(WINDOW *win, int direction, LinkedList* snakey) {
    mvwaddch(win, snakey->head->segment.y, snakey->head->segment.x, '@');
    for (int i = 1; i < SNAKEY_LENGTH - 2; i++) {
        Segment tempSeg = getElementLinkedList(snakey, i);
        mvwaddch(win, tempSeg.y, tempSeg.x, '#');
    }
    mvwaddch(win, snakey->tail->segment.y, snakey->tail->segment.x, 'o');

    int x = snakey->tail->segment.x;
    int y = snakey->tail->segment.y;
    switch (direction) {
        case UP:
            y - 1 == 0 ? endnum = 1 : y--;
            break;
        case DOWN:
            y + 1 == WORLD_HEIGHT - 1 ? endnum = 1 : y++;
            break;
        case RIGHT:
            x + 1 == WORLD_WIDTH - 1 ? endnum = 1 : x++;
            break;
        case LEFT:
            x - 1 == 0 ? endnum = 1 : x--;
            break;
        default:
            break;
    }

    if(endnum == 1){
        return 1;
    }

    snakey->tail->segment.x = x;
    snakey->tail->segment.y = y;

    mvwaddch(win, y, x, '@');
    box(win, 0 , 0);
    return 0;
}

//Check collide with self

/**
 * Check_food() - checks to see if the snake has collided with any food
 * @param snakey - the linked list containing the snake
 * @param x - the x coord of the food
 * @param y - the y coord of the food
 * @return 1 if collided, 0 else
 */
int check_food(LinkedList* snakey, int x, int y){
    if(snakey->head->segment.x == x && snakey->head->segment.y == y){
        return 1;
    }
    return 0;
}

//Append
void grow_snakey(LinkedList* snakey){
    Segment newSeg = {snakey->head->segment.x, snakey->head->segment.y};
    appendElementLinkedList(snakey, newSeg);
}

/**
 * Generate_food() - generates food on the screen
 * @param win - the WINDOW in which the food appears
 */
Segment generate_food(WINDOW* win){
    int x = (rand()) % (WORLD_WIDTH -1);
    int y = (rand()) % (WORLD_HEIGHT -1);
    mvwaddch(win, y, x, 'f');
    Segment rand_seg = {x, y};
    return rand_seg;
}