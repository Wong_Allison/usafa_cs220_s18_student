//
// Created by C20Allison.Wong on 3/8/2018.
//
#ifndef USAFA_CS220_S18_STUDENT_PEX2_H
#define USAFA_CS220_S18_STUDENT_PEX2_H

int endnum = 0;


int move_snakey(WINDOW *win, int direction, LinkedList* snakey);

int check_food(LinkedList* snakey, int x, int y);

void grow_snakey(LinkedList* snakey);

Segment generate_food(WINDOW* win);




#endif //USAFA_CS220_S18_STUDENT_PEX2_H
