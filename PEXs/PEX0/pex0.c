/** pex0.c
 * ==========================================================
 * Name: Allison Wong
 * Section: T3A
 * Project: PEX 0
 * Documentation Statement: None
 * ===========================================================
 */

#include "pex0.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>



int main() {

    // You may add code to test your functions above this comment, however be sure to not change
    // the test code below.  Please uncomment the code below when you are ready to test your PEX.
    // Once you are satisfied that you've got it working correctly comment out any code you've added
    // above and leave the test code below uncommented.

    printf("hamming = %d\n",hammingDistance("AAA","CC")); // result= -1
    printf("hamming = %d\n",hammingDistance("ACCT","ACCG")); //result= 1
    printf("hamming = %d\n",hammingDistance("ACGGT","CCGTT")); //result= 2

    printf("simularity = %0.3f\n",similarityScore("CCC","CCC")); // result= 1.0
    printf("simularity = %0.3f\n",similarityScore("ACCT","ACCG")); // result= 0.75
    printf("simularity = %0.3f\n",similarityScore("ACGGT","CCGTT")); // result= 0.6
    printf("simularity = %0.3f\n",similarityScore("CCGCCGCCGA","CCTCCTCCTA")); // result= 0.7

    printf("# matches = %d\n",countMatches("CCGCCGCCGA", "TTT", 0.6)); // result= 0
    printf("# matches = %d\n",countMatches("CCGCCGCCGA", "CCG", 0.2)); // result= 8
    printf("# matches = %d\n",countMatches(MOUSEDNA, "CGCTT", 0.5)); // result= 36
    printf("# matches = %d\n",countMatches(HUMANDNA, "CGC", 0.3)); // result= 215

    printf("best match = %0.3f\n",findBestMatch("CCGCCGCCGA", "TTT")); // result= 0.0
    printf("best match = %0.3f\n",findBestMatch("CTGCCACCAA", "CCGC")); // result= 0.75
    printf("best match = %0.3f\n",findBestMatch(MOUSEDNA, "CGCTT")); // result= 0.8
    printf("best match = %0.3f\n",findBestMatch(HUMANDNA, "CGC")); // result= 1.0

    printf("likely genome match = %d.\n",findBestGenome(CATDNA,HUMANDNA,MOUSEDNA,"CTTTAGGCCGGTT")); //result= 1
    printf("likely genome match = %d.\n",findBestGenome(CATDNA,HUMANDNA,MOUSEDNA,"CTTTAGGCCGGG")); //result= 2
    printf("likely genome match = %d.\n",findBestGenome(CATDNA,HUMANDNA,MOUSEDNA,"CTTAATTCTTTT")); //result= 3
    printf("likely genome match = %d.\n",findBestGenome(CATDNA,HUMANDNA,MOUSEDNA,"CTTTAG")); //result= 0

    return 0;
}

/**
 * hammingDistance()
 * Finds the number of mismatches between two strings of DNA.
 * @param char* str1: the first string to be compared with
 * @param char* str2: the second string to compare to str1
 * @return int hamDist: the number of mismatches between the two strings
 */
int hammingDistance(char* str1, char* str2) {
    //Function returns -1 if the strings are not of the same length
    if (strlen(str1) != strlen(str2)) {
        return -1;
    }
    int hamDist = 0;
    size_t count = strlen(str1); //Finds length of the strings

    //hamDist increments if the characters of each string, at index i, don't match
    for (int i = 0; i < (int) count; i++) {
        if (str1[i] != str2[i]) {
            hamDist++;
        }
    }
    return hamDist;
}

/**
 * similarityScore()
 * Calculates the similarity score between two sequences of DNA. The higher the score, the more similar they are.
 * similarityScore  = (sequenceLength - hammingDistance)/sequenceLength
 * sequenceLength is the count of the number of chemical bases that are in the sequence.
 * @param char* seq1: the first sequence of DNA
 * @param char* seq2: the second sequence of DNA
 * @return float simScore: the calculated similarity score (formula is above)
 */
float similarityScore(char* seq1, char* seq2){
    //Returns -1 if the two sequences are not of equal length
    if(strlen(seq1) != strlen(seq2)){
        return -1;
    }
    int sequenceLength = (int)strlen(seq1);     //Finds the length of the strings
    float simScore = (float)(sequenceLength - hammingDistance(seq1, seq2))/sequenceLength;  //Calculates the score as a
                                                                                                //float
    return simScore;
}

/**
 * countMatches()
 * Counts the number of times a given sequence has an exact match in the given genome
 * @param char* genome: the genome for the given sequence to be compared to
 * @param char* seq: the sequence to compare to the genome
 * @param float minScore: the minimum similarity score that the sequence must have to count as a match
 * @return int matches: the number of matches found
 */
int countMatches(char* genome, char* seq, float minScore){
    //Finds the lengths of the genome and the sequence
    size_t genome_len = strlen(genome);
    size_t seq_len = strlen(seq);
    int matches = 0;

    //Increments matches for every time the sequence's similarity score is greater than the minimum score
    for (int i = 0; i < genome_len; i++) {

//        strncpy method
//        char* newer_genome[seq_len+1];
//        strncpy(&newer_genome, &genome[i], seq_len);
//        newer_genome[seq_len] = '\0';

        char* new_genome = strndup(genome+i, seq_len); //Another way that works

        float simScore = similarityScore(new_genome, seq);
        if(simScore >= minScore){
            matches++;
        }
    }
    return matches;
}

/**
 * findBestMatch()
 * Finds the highest similarity score at any position in the genome with the given sequence
 * @param char* genome: the genome for the given sequence to be compared to
 * @param char* seq: the sequence to compare to the genome
 * @return float highScore: the highest similarity score found
 */
float findBestMatch(char* genome, char* seq){
    //Finds the lengths of the genome and the sequence
    size_t genome_len = strlen(genome);
    size_t seq_len = strlen(seq);
    float highScore = 0;

    //Looks at every position for the highest similarity score
    for (int i = 0; i < genome_len; i++) {

//        strncpy method
//        char* newer_genome[seq_len+1];
//        strncpy(&newer_genome, &genome[i], seq_len);
//        newer_genome[seq_len] = '\0';

        char *new_genome = strndup(genome + i, seq_len);

        float simScore = similarityScore(new_genome, seq);
        if(simScore > highScore){
            highScore = simScore;
        }
    }
    return highScore;
}

/**
 * findBestGenome(
 *
 * @param char* genome1: the first genome for the unknownSeq to compare to
 * @param char* genome2: the second genome for the unknownSeq to compare to
 * @param char* genome3: the third genome for the unknownSeq to compare to
 * @param char* unknownSeq: the unknownSeq to find matches in the genomes
 * @return int: the number of the genome with the highest similarity score
 */
int findBestGenome(char* genome1, char* genome2, char* genome3, char* unknownSeq){
    //Finds the highest similarity score of the unknown sequence in each genome
    float genome1_match = findBestMatch(genome1, unknownSeq);
    float genome2_match = findBestMatch(genome2, unknownSeq);
    float genome3_match = findBestMatch(genome3, unknownSeq);

    //Returns the number of the genome that has the highest similarity score
    if(genome1_match > genome2_match && genome1_match > genome3_match){
        return 1;
    }
    if(genome2_match > genome1_match && genome2_match > genome3_match){
        return 2;
    }
    if(genome3_match > genome2_match && genome3_match > genome1_match){
        return 3;
    }
    //Returns zero if two or more genomes
    if((genome1_match == genome2_match) || (genome2_match == genome3_match) || (genome1_match == genome3_match)){
        return 0;
    }

}
