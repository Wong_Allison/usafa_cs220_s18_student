/** search.c
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: PEX 1
* Purpose: Binary and linear searching functions
* ===========================================================
*/
#include "search.h"

/**
 * binSearch finds the location of a given item using the binary search method
 * @param int array[]: the array to be searched
 * @param int lBound: the lower bound
 * @param int rBound: the upper bound
 * @param int item: the item whose position is to be found
 * @return the location of the item in the given array
 */
//Returns location of the item in the given array, lower bound, and upper bound, otherwise return -1
int binSearch(int array[], int lBound, int rBound, int item){
    // first base case - we haven't found it then lBound > rBound so return -1
    if (lBound <= rBound) {
        // calculate a mid point
        int mid = lBound + (rBound - lBound) / 2;

        if (array[mid] == item) {
            return mid;
            // wasn't found so look in left half or right recursively
        }
        else if (array[mid] > item) {
            return binSearch(array, lBound, mid - 1, item);
        }
        return binSearch(array, mid + 1, rBound, item);
    }
    return -1;
}

/**
 * linearSearch finds the position of an item in a given array using the linear search method
 * @param int array[]: the array to be searched
 * @param int N: the size of the array
 * @param int item: the item whose position is to be found
 * @return the location of the item in the given array
 */
int linearSearch(int array[], int N, int item){
    //search for item in the array linearly
    for (int i = 0; i < N; i++) {
        if(array[i] == item) {
            return i;
        }
    }
    //item is not present in the array
    return -1;
}