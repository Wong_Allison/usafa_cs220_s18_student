/** sort.c
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: PEX 1
* Purpose: Selection, insertion, merge, and quick sort functions
* ===========================================================
*/
#include <stdio.h>
#include "sort.h"

//For testing purposes
//int main() {
//    int array[] = {10, 8, 7, 9, 1, 5};
//    int n = sizeof(array)/sizeof(array[0]);
//
//    printf("Selection: \n");
//    printArray(array, n);
//    selSort(array, n, 0);
//    printArray(array, n);
//
//    int array1[] = {10, 8, 7, 9, 1, 5};
//    printf("Insertion: \n");
//    printArray(array1, n);
//    insertSort(array1, n);
//    printArray(array1, n);
//
//    int array3[] = {10, 8, 7, 9, 1, 5};
//    printf("Quick: \n");
//    printArray(array3, n);
//    quickSort(array3, 0, n-1);
//    printArray(array3, n);
//
//    int array2[] = {10, 8, 7, 9, 1, 5};
//    printf("Merge: \n");
//    printArray(array2, n);
//    mergeSort(array2, 0, n-1, n);
//    printArray(array2, n);
//
//    return 0;
//}

/**
 * selSort function sorts the array using the selection method
 * @param int array[]: the array to be sorted
 * @param int N: size of the array
 * @param int order: 0 if to be sorted ascending, 1 if to be sorted descending
 */
void selSort(int array[], int N, int order){
    int smallest = 0;
    int smallIndex = 0;

    //If to be sorted ascending
    if(order == 0){
        // process the array from left to right
        for (int i = 0; i < N; i++) {
            // look for smallest value in the array
            smallest = array[i];
            smallIndex = i;
            for (int j = i + 1; j < N; j++) {
                if (array[j] < smallest) {
                    smallest = array[j];
                    smallIndex = j;            }
            }
            // once found swap it with the value in the ith
            // position
            swap(&array[i],&array[smallIndex]);
        }
    }
    //If to be sorted descending
    if(order == 1){
        // process the array from left to right
        for (int i = 0; i < N; i++) {
            // look for smallest value in the array
            smallest = array[i];
            smallIndex = i;
            for (int j = i + 1; j < N; j++) {
                if (array[j] > smallest) {
                    smallest = array[j];
                    smallIndex = j;            }
            }
            // once found swap it with the value in the ith
            // position
            swap(&array[i],&array[smallIndex]);
        }
    }
}

/**
 * swap function swaps two elements of an array
 * @param int x: first to be swapped with the second
 * @param int y: second to be swapped with the first
 */
void swap(int *x, int *y){
    int temp = *x;
    *x = *y;
    *y = temp;
}

/**
 * insertSort sorts the array using the insertion method
 * @param int array[]: the array to be sorted
 * @param int N: size of the array
 */
void insertSort(int array[], int N){
    int insertElem = 0;
    for (int i = 1; i < N; i++ ) {
        insertElem = array[i]; // the item to insert
        int j = i-1;
        while (j >= 0 && array[j] > insertElem) {   // move values up until we find the insertion
            array[j+1] = array[j];                  // point
            j--;
        }
        array[j+1] = insertElem;    // put the value in the empty slot
    }
}

/**
 * mergeSort sorts the array using the merge method
 * @param int array[]: the array to be sorted
 * @param int lBound: the lower bound
 * @param int rBound: the upper bound
 * @param int N: the size of the array
 */
void mergeSort(int array[], int lBound, int rBound, int N){
    if(lBound < rBound) {
        int mid = (lBound + rBound - 1)/2;

        mergeSort(array, lBound, mid, N);
        mergeSort(array, mid + 1, rBound, N);

        merge(array, lBound, mid, rBound, N);
    }
}

void merge(int array[], int lBound, int mid, int rBound, int N){
    int tempArray[N];
    // Copy the first sublist into the tempArray
    for (int j = lBound; j <= mid; j++) {
        tempArray[j] = array[j];
    }
    // Copy the second sublist into the tempArray
    for (int j = mid + 1, k = rBound; j <= rBound; j++, k--) {
        tempArray[k] = array[j];
    }

    // Merge the two sublists
    int j = lBound;
    int k = rBound;
    int i = lBound;
    while (j <= k) {
        if (tempArray[j] < tempArray[k]) {
            array[i] = tempArray[j];
            j++;
        } else {
            array[i] = tempArray[k];
            k--;
        }
        i++;
    }
}

/**
 * quickSort sorts the array using the quick method
 * @param int array[]: the array to be sorted
 * @param int lBound: the lower bound
 * @param int rBound: the upper bound
 */
void quickSort(int array[], int lBound, int rBound){
    if (lBound < rBound) {
        int divPt = partition(array, lBound, rBound);
        quickSort(array, lBound, divPt - 1);
        quickSort(array, divPt + 1, rBound);
    }
}

int partition(int array[], int lBound, int rBound){
    int pivot = array[lBound];
    int lastSmall = lBound;
    for (int i = lBound + 1; i <= rBound; i++) {
        if (array[i] < pivot) {
            lastSmall++;
            swap(&array[lastSmall], &array[i]);
        }
    }
    swap(&array[lBound], &array[lastSmall]);
    return lastSmall;  //return the division point
}

//Used in main to test the sorting functions
void printArray(int array[], int size){
    for (int i = 0; i < size; ++i) {
        printf("%d ", array[i]);
    }
    printf("\n");
}
