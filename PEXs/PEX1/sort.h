/** sort.h
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: PEX 1
* Purpose: Defining selection, insertion, merge, and quick sort
* ===========================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_PEX1_H
#define USAFA_CS220_S18_STUDENT_PEX1_H

/** -------------------------------------------------------------------
 * selSort() - Perform a selection sort on an array
 * @param array - the array to sort
 * @param N - the size of the array
 * @param order - 0 for ascending, 1 for descending
 */
void selSort(int array[], int N, int order);
void swap(int *x, int *y);

/** -------------------------------------------------------------------
 * insertSort() - Perform a insertion sort on an array
 * @param array - the array to sort
 * @param N - the size of the array
 */
void insertSort(int array[], int N);

/** -------------------------------------------------------------------
 * mergeSort() - Perform a mergesort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 * @param N - the size of the array
 */
void mergeSort(int array[], int lBound, int rBound, int N);
void merge(int array[], int lBound, int mid, int rBound, int N);

/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound);
int partition(int array[], int low, int high);

//void printArray(int array[], int size);

#endif //USAFA_CS220_S18_STUDENT_PEX1_H
