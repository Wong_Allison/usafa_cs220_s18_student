/** lab01.c
* ===========================================================
* Name: Allison Wong / 7 Jan 2018
* Section: T3/4
* Project: Lab 1 - Basic C Functionality
* Purpose: Learn C basics
* ===========================================================
* Instructions:
 * Complete the tasks below without using CLion!
 * Use notepad and compilation from the Cygwin command shell (bash).
 *
 * From a Cygwin command shell type "gcc filename.c -o lab01.exe" to compile
 * and link your program.
 *
 * Use ./lab01.exe to execute your program
*/
#include <stdio.h>
#include  <time.h>

int main() {
	/*Example 0
	int sub1, sub2, sub3, sub4;
	float percent;
	printf ("Enter the marks for four subjects:");
	scanf ("%d %d %d %d",&sub1, &sub2, &sub3, &sub4);
	percent = (sub1 + sub2 + sub3 + sub4)/ 400.00*100;
	printf ("The percentage = %f", percent);*/ 

    /* TASK A - Variable Basics
     * 0) Don't for get that you will need to add #include
     *    directives for any libraries you wish to use.
     * 1) prompt the user for a two integers x and y
     * 2) output the first value divided by the second value
     * 3) are you surprised by the output? (hopefully not)
     * 3) declare a float variable named quotient that is the
     *    first integer divided by the second integer value
     * 3) output quotient
     *    Note: Be sure to full precision division!  Do this by
     *    type converting an int to a float
     *    use -> (float) intVar (this is called a cast)
	 * Check error
     */
	 
	/*
	int in1, in2;
	printf("Enter two integers:");
	scanf ("%d %d",&in1, &in2);
	printf("The numbers are %d and %d, and their quotient is %d", in1, in2, in1/in2)
	float quotient = (double) (in1/in2);
	printf("The actual quotient is %f \n", quotient);
	*/	
	

    /* TASK B - Conditional Operator
     * 1) prompt user for two values x, y
     * 2) Use the conditional operator (? :) to print x if
     *    it is larger than y otherwise print y
	 * Check
     */
	 
	/*
	int val1, val2;
	printf("Enter two values:");
	scanf("%d %d", &val1, &val2);
	int larger = (val1>val2)?val1:val2;
	printf("%d", larger);
	*/

    /* TASK C - Print divisors
     * 1) ask user for a lowerBound and an upperBound
     * 2) from the lower to the upper bound print out
     *    the multiples of 3 using a for loop
     * 3) accomplish part 2 again using a while loop and then a
     *    do while loop
	 * Check
     */
	 
	 /*
	 int lowerBound, upperBound;
	 printf("Enter a lowerBound and an upperBound: ");
	 scanf("%d %d", &lowerBound, &upperBound);
	 for(lowerBound; lowerBound < upperBound; lowerBound++){
		 if(lowerBound	%3==0){
			 printf("%d \n", lowerBound);
		}
	 }
	 */

    /* TASK D - Grade Scale
     * 1) prompt the user for an integer grade value 4=A, 3=B and so on
     * 2) use a switch statement to output the corresponding letter grade
     *    Note: use default case for "F"
     */
	 
	/*
	int user;
	scanf("%d", user);
	switch(user){
		Case4:
			printf("A");
			break;
		Case3:
			printf("B");
			break;
		Case2:
			printf("C");
			break;
		Case1:
			printf("D");
			break;
		default:
			print("F");
	}
	*/

    /* TASK E - Guessing game in C
     * 1) generate a random integer in c, the magic number
     *          * rand() in stdlib.h may be useful
     * 2) prompt the user for an integer between 1 and 100 this will
     *    be the users guess of the magic number
     * 3) print appropriate messages if the guess is low or high relative
     *    to the magic number and allow the user to guess again
     * 4) if the user guesses the magic number output the number of guesses
     *    made -- game over
     */
	 #include <time.h>
	 #include <stdlib.h>
	 int count = 0;
	 int guess, ans;
	 srand(time(0));
	 ans = rand()%100+1; //range 1-100
	 
	 do{
		 printf("Guess a number 1-100 \n");
		 scanf("%d", &guess);
		 if(guess!=ans){
			 guess>ans?printf("Your guess was too high.\n"):printf("Your guess was too low.\n");
		 }
		 count++;
	 }
	 while(guess!=ans);
	 printf("Correct: %d \n", ans);

}