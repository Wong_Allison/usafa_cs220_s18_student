/* lab03.h
* ===========================================================
* Name: Troy Weingart, 8 Jan 18
* Section: n/a
* Project: Lesson 3 Lab
* Purpose: Practice with header files & recursion
* ===========================================================
 */
#ifndef USAFA_CS220_S18_STUDENT_LAB03_H
#define USAFA_CS220_S18_STUDENT_LAB03_H

#include <stdbool.h>

/* ----------------------------------------------------------
 * triBlocksRec() - computes the number of blocks in a triangle
 * stack
 * @param rows is the number of rows in the stack of blocks
 * @return number of rows
 * ----------------------------------------------------------
 */
int triBlocksRec(int rows);

/* ----------------------------------------------------------
 * sumDigitsRec() adds the individual digits of an integer
 * @param num the integer to sum
 * @return the sum
 * ----------------------------------------------------------
 */
int sumDigitsRec(int num);

/* ----------------------------------------------------------
 * arrayContainsRec() determines if a given number is
 * contained in an arry
 * @param nums the array of integers
 * @param index the start index of operation (typically the end)
 * @param value the number you are looking for
 * @return true if contains value
 * ----------------------------------------------------------
 */
bool arrayContainsRec(int nums[], int index, int value);

#endif //USAFA_CS220_S18_STUDENT_LAB03_H
