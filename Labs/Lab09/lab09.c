/** lab09.c
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: Lab 9 - Merge / Quick / Shell Sorts
 *
 * Merge - split in half, and again and again, then sort on the way back up the tree
* ===========================================================
* Instructions:
*    1) Complete TASKS Below
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab09.h"

// Local function prototypes
void fillArray(int array[]);
void swap(int* x, int* y);
void merge(int array[], int lBound, int mid, int rBound);
int partition(int array[], int lBound, int rBound);

int main() {
    int nums[N];

     /* TASK 0 - implement the function, mergeSort() below
      *  Help - the merge prototype is given to you above
      */
//    fillArray(nums);
//    mergeSort(nums,0,N-1);

    /* TASK 1 - implement the function, quickSort() below
     * Help - the partition prototype is given to you above
     */
    fillArray(nums);
    printArray(nums);
    quickSort(nums,0,N-1);
    printArray(nums);

    /* TASK 2 - implement the function, shellSort() below
     * using diminishing step values of 5,3,1
     */
//    fillArray(nums);
//    shellSort(nums);

    return 0;
}

void printArray(int array[]){
    for (int j = 0; j < N-1 ; j++) {
        printf("%d ", array[j]);
    }
    printf("\n");
}

/**
 * merge() - Given two sorted sublists array[lBound..mid] and array[mid+1..rBound],
 * merge them into a single sorted list in array[lBound..rBound]
 * @param array - the array to sort
 * @param lBound - the lowest index of the first sublist
 * @param mid - the highest index of the first sublist
 * @param rBound - the highest index of the second sublist
 */
void merge(int array[], int lBound, int mid, int rBound) {
    int i, j, k;
    int n1 = mid - lBound + 1;
    int n2 = rBound - mid;

    int L[n1], R[n2];

    for(i = 0; i < n1; i++){
        L[i] = array[lBound + i];
    }
    for(j = 0; j < n2; j++){
        R[j] = array[mid + 1 + j];
    }

    i = 0;
    j = 0;
    k = lBound;
    while(i < n1 && j < n2){
        if(L[i] <= R[j]){
            array[k] = L[i];
            i++;
        }
        else{
            array[k] = R[j];
            j++
        }
        k++;
    }
    while(i < n1){
        array[k] = L[i];
        i++;
        k++;
    }

    while(j < n2){
        array[k] = R[j];
        j++;
        k++;
    }
}

/** -------------------------------------------------------------------
 * mergeSort() - Perform a mergesort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void mergeSort(int array[], int lBound, int rBound) {
    if(1 < rBound){
        int m = lBound + (rBound - 1)/2;

        mergeSort(array, lBound, m);
        mergeSort(array, m + 1, rBound);
        merge(array, lBound, m, rBound);
    }
}

/**
 * partition() - partition the sublist in to two lists
 * of elements larger and smaller than the selected pivot
 * @param array - the array to sort
 * @param lBound - the left bound of the sublist
 * @param rBound - the right bound of the sublist
 */
int partition(int array[], int lBound, int rBound) {
    int pivot = array[lBound];
    int lastSmall = lBound;
    for (int i = lBound + 1; i <= rBound; i++) {
        if (array[i] < pivot){
            lastSmall++;
            swap(&array[lastSmall], &array[i]);
        }
    }
    swap(&array[lBound], &array[lastSmall]);
    return lastSmall;
}


/** -------------------------------------------------------------------
 * quickSort() - Perform a quick sort on a portion of an array, from lBound
 * to rBound
 * @param array - the array to sort
 * @param lBound - the starting index of the sublist to sort
 * @param rBound - the ending index of the sublist to sort
 */
void quickSort(int array[], int lBound, int rBound) {
    if(lBound < rBound){
        int divPt = partition(array, lBound, rBound);
        quickSort(array, lBound, divPt-1);
        quickSort(array, divPt-1, rBound);
    }
}

/** -------------------------------------------------------------------
 * shellSort() - Perform a shell sort on a an array
 * @param array - the array to sort
 */
void shellSort(int array[]) {
    for(int gap = N/2; gap > 0; gap /= 2){
        for(int i = gap; i < N; i += 1){
            int temp = array[i];
            for(int j = i; j >= gap && array[j - gap] > temp; j -= gap){
                array[j] = array[j-gap];
                array[j] = temp;
            }
        }

    }
    return 0;
}

//swaps two integer values
void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

//fills an array of size n with random values
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    // fill array with random ints from 0 to 99
    for (int i = 0; i < N; i++) {
        array[i] = (int) random() % 100;
    }
}