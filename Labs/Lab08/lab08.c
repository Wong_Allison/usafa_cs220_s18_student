/** lab08.c
* ===========================================================
* Name: Allison Wong
* Section: T3A
* Project: Lab 8 - Selection / Bubble / Insertion Sorts
* ===========================================================
* Instructions:
*    1) Complete TASKS Below
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "lab08.h"

// Local function prototypes follow
void fillArray(int array[]);
void swap(int* x, int* y);

//Insertion
//Selection Sort
//Bubble Sort

int main() {
    int nums[n];

    // fill the array with random numbers
    //fillArray(nums);
    
    /* TASK 0 - implement the function, selSort() below
     * 0) Call the function and verify that it works
     */
//    for (int i = 0; i < n-1; i++) {
//        printf("%d ", nums[i]);
//    }
//    printf("\n");
//    selSort(nums);
//    for (int j = 0; j < n-1; j++) {
//        printf("%d ", nums[j]);
//    }

    /* TASK 1 - implement the function, bubSort() below
     * 0) Refill the array to get a new random set of 
     * numbers.
     * 1) Call the function and verify that it works
     */
//    for (int i = 0; i < n-1; i++) {
//        printf("%d ", nums[i]);
//    }
//    printf("\n");
//    bubSort(nums);
//    for (int j = 0; j < n-1; j++) {
//        printf("%d ", nums[j]);
//    }

    /* TASK 2 - implement the function, insertSort() below
     * 0) Refill the array to get a new random set of 
     * numbers.
     * 1) Call the function and verify that it works
     */
//    for (int i = 0; i < n-1; i++) {
//        printf("%d ", nums[i]);
//    }
//    printf("\n");
//    insertSort(nums);
//    for (int j = 0; j < n-1; j++) {
//        printf("%d ", nums[j]);
//    }
    int a[] = {20, 30, 37, 87, 10};
    int b = *(a+3);
    printf("%d\n", b);

    *(a+3) = *(a + 3) + 10;
    printf("%d\n", a[3]);

    int c = 0;
    int* x = a + 1;
    while(c<4){
        printf("%d\n", *x + *(x-1));
        x++;
        c++;
    }

    return 0;
}

// Insertion Sort
void insertSort(int array[]) {
    for (int i = 1; i < n-1; i++) {
        int key = array[i];
        int j = i-1;

        while(j >= 0 && array[j] > key){
            array[j+1] = array[j];
            j = j-1;
        }
        array[j+1] = key;
    }
}

// Bubble Sort
void bubSort(int array[]){
    for (int i = 0; i < n-1; i++){
        for(int j = 0; j < n-i-1; j++){
            if(array[j] > array[j+1]){
                swap(&array[j], &array[j+1]);
            }
        }
    }
}

// Selection Sort
void selSort(int array[]) {
    for (int i = 0; i < n-1; i++) {
        int min = i;
        for (int j = i + 1; j < n-1; j++) {
            if (array[j] < array[min]) {
                min = j;
            }
        }
        swap(&array[i], &array[min]);
    }
}

//swaps two integer values
void swap(int* x, int* y) {
    int temp;

    temp = *y;
    *y = *x;
    *x = temp;
}

//fills an array of size n with random values
void fillArray(int array[]) {
    //set up for and then seed random number
    //generator
    struct timespec time;
    clock_gettime(CLOCK_REALTIME,&time);
    srandom((unsigned) (time.tv_nsec ^ time.tv_sec));

    // fill array with random ints from 0 to 29
    for (int i = 0; i < n; i++) {
        array[i] = (int) random() % 30;
    }
}

int countletterRecursive(char str[], char letter){
    int length = (int)sizeof(str);
    printf("%d", length);
    if(length > 0){
        if(str[0] == letter) {
            return 1 + countletterRecursive(str[1], letter);
        }
        printf("%c", str[1]);
        return countletterRecursive(str[1], letter);
    }
}