/** lab23.c
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Header for BST library
* =================================================================
*/
#include <stdio.h>
#include <stdlib.h>
#include "bstAsLinkedDS.h"

/* Instructions - Complete the following tasks
 *
 * Task0 - complete insertBST as defined in the
 * header file.  Test your implementation.  Note:
 * Use a recursive helper function to implement this
 * function.
 *
 * Task1 - complete the 3 traversals as defined
 * in the header file.  Test your implementation.
 *
 *
 */
int main() {
    BSTnode* root = NULL;
    root = insertBST(root,5);
    root = insertBST(root,10);
    root = insertBST(root,1);
    root = insertBST(root,7);
    root = insertBST(root,3);
    root = insertBST(root,6);
    printInorderBST(root);printf("\n");
    printPreorderBST(root);printf("\n");
    printPostorderBST(root);printf("\n");
}
//Pre: valid BSTNode* root
//Post: valid BST, if root==NULL, then create BST at root
                //if not NULL, add as correct leaf per BST rules

BSTnode* insertBST(BSTnode* root, int item) {
    BSTnode *newNode = malloc(sizeof(BSTnode));
    newNode->data = item;
    newNode->left = NULL;
    newNode->right = NULL;
    return insertBSTRec(root, newNode);
//    if(root == NULL){
//        return newNode;
//    }
//    else{
//        return insertBSTRec(root, newNode);
//    }
}

BSTnode* insertBSTRec(BSTnode* root, BSTnode* newNode){
    if(root == NULL){
        return newNode;
    }

    if(newNode->data < root->data){
        root->left = insertBSTRec(root->left, newNode);
    }
    else{
        root->right = insertBSTRec(root->right, newNode);
    }
    return root;
}


void printInorderBST(BSTnode* nodePtr){
    if(nodePtr != NULL){
        printInorderBST(nodePtr->left);
        printf("%d", nodePtr->data);
        printInorderBST(nodePtr->right);
    }
}

void printPreorderBST(BSTnode* nodePtr){
    if(nodePtr != NULL){
        printf("%d", nodePtr->data);
        printPreorderBST(nodePtr->left);
        printPreorderBST(nodePtr->right);
    }
}

void printPostorderBST(BSTnode* nodePtr){
    if(nodePtr != NULL){
        printPreorderBST(nodePtr->left);
        printPreorderBST(nodePtr->right);
        printf("%d", nodePtr->data);
    }
}