/** listAsLinkedList.h
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Header for Linked List Library
*
* Instructions:  Complete the implementation file for this header file
* (For lab 16 on lesson 16 only complete part 1).  Also create a menu
* system like lab 13 to test your listAsLinkedList library
*
*
*
* =================================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#include <stdbool.h>
#include <listAsLinkedListpt1.c>

// Define a node of the linked list
typedef struct node {
    int data;
    struct node* next;
} Node;

// Define the type (meta-data) that manages the linked list of nodes
typedef struct {
    Node* head;
    Node* tail;
    int numberOfElements;
} LinkedList;

// Functions that manipulate a linked list
// lab 16 part 1
LinkedList* createLinkedList();
void deleteLinkedList(LinkedList* list);
void appendElementLinkedList(LinkedList* list, int element);
int lengthOfLinkedList(LinkedList* list);
void printLinkedList(LinkedList* list);
int getElementLinkedList(LinkedList* list, int position);
void changeElementLinkedList(LinkedList* list, int position, int newElement);
// lab 16 part 2 (lab18)
void deleteElementLinkedList(LinkedList* list, int position);
void insertElementLinkedList(LinkedList* list, int position, int element);
int findElementLinkedList(LinkedList* list, int element);

void deleteElementLinkedList(LinkedList* list, int position){

}

void insertElementLinkedList(LinkedList* list, int position, int element){
    if(position > list->numberOfElements){
        return;
    }
    //Create new node
    Node* newNode = malloc(sizeof(Node));
    newNode->data = element;
    newNode->next = NULL;
    //Insert node based on cases
    if(list->numberOfElements = 0){
        list->head = newNode;
        list->tail = list->head;
        list->numberOfElements++;
    }
    else if(position == 0){
        newNode->next = list->head;
        list->head = newNode;
        list->numberOfElements++;
    }
    else if(position = list->numberOfElements+1){
        appendElementLinkedList(list, element);
    }
    else{
        Node* tempPtr = list->head;
        int i = 0;
        while(i<position-1){
            i++;
            tempPtr = tempPtr->next;
        }
        newNode->next = tempPtr->next;
        tempPtr->next = newNode;
        list->numberOfElements++;
    }
}

int findElementLinkedList(LinkedList* list, int element){

}

// Optional part 3
void selSortLinkedList(LinkedList *list);
void insertSortLinkedList(LinkedList* list);

#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
