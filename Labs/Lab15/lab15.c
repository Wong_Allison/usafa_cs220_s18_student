/** lab15.c
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Lab15 - Dynamic Memory Allocation
* =================================================================
*
* Instructions:  In this lab you will practice allocating memory
* dynamically, using the c library functions malloc() and
* realloc().  Specifically you will allocate an array using malloc()
* and then dynamically grow this array using realloc() as it is filled
* with random integers.
*
* Complete the tasks below.
*/

#define INIT_SIZE 1000

//local function prototypes

int main() {

    /** TASK 0 - finish the seedGenerator() function.
     * This function seeds the random number
     * generator (see lab 9's fill array function).  This function
     * should only allow the srand() to be called once. Call the
     * function below.
     */


    /** TASK 1 - Allocate an array of INIT_SIZE integers
     * using the malloc() function call.
     */
    nums = (int *) malloc(INIT_SIZE* sizeof(int));

    /** TASK 2 - declare and initialize an int variable to
     * hold the size of the data set of random integers.
     * The size should vary between 1 and 100000.
     */
    int num = (int)sizeof(nums);

    /** TASK 3 - complete the fillArray() function implementation
     * and call it.
     * part 1 - implement fillArray() using realloc() (header below)
     *    Note: double the array in size when it is reallocated
     * part 2 - call the function and print out a message
     * that includes the initial size of the array, the
     * randomly generated data set size, and the number
     * of times doubled.
     */

    free(nums);

    return 0;
}

/** ----------------------------------------------------------
 * fillArray() - fills array with random ints its size is
 * specfied by dataSetSize
 * @param array is a ptr to an array of integers
 * @param arraySize is the size of the array of integers
 * @param dataSetSize is the final size of the array of integers
 * which can be smaller, equal to, or larger than the array's initial
 * size
 * @return the number of times the initial array was doubled in size
 * to support the larger data set
 * ----------------------------------------------------------
 */
int fillArray(int *array, int arraySize, int dataSetSize) {

}

/** ----------------------------------------------------------
 * seedGenerator() - seeds the random number generator only once.
 * ----------------------------------------------------------
 */
void seedGenerator() {

}