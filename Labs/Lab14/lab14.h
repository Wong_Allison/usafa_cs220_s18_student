//
// Created by C20Allison.Wong on 2/12/2018.
//

#ifndef USAFA_CS220_S18_STUDENT_LAB14_H
#define USAFA_CS220_S18_STUDENT_LAB14_H

#endif //USAFA_CS220_S18_STUDENT_LAB14_H

void fillArray(int* array);

void printArray(int* array);

void incrArray(int* array, int add);

void insertSort(int* array);