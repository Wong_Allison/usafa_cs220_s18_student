/** lab14.c
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Lab14 - Pointer use in C
* =================================================================
*
* Instructions: For this lab you can borrow code from prior labs to
* accomplish the tasks below
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lab14.h"
int N = 5;

int main(){
    int array[N];
    fillArray(array);
    printArray(array);

    incrArray(array, 1);
    printArray(array);

    insertSort(array);
    printArray(array);
    return 0;
}

void fillArray(int* array) {
    for (int i = 0; i < N; i++) {
        *(array+i) = (int)random() % 100;
    }
}

void printArray(int* array){
    static int seedDone = 0; //static variables retain their value between calls

    // modified so the seed is only done once
    if (!seedDone) {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        srandom((unsigned) (time.tv_nsec ^ time.tv_sec));
        seedDone = 1;
    }

    printf("\n");
    for (int i = 0; i < N; i++) {
        printf("\n%d", *(array+i));
    }
}

void incrArray(int* array, int add){
    for (int i = 0; i < N; i++) {
        *(array+i) = *(array+i) + add;
    }
}

void insertSort(int* array){
    for (int i = 0; i < N; i++) {
        int key = *(array+i);
        int j = i-1;

        while(j>=0 && *(array+j) > key) {
            *(array+j+1) = *(array+j);
            j--;
        }
        *(array+j+1) = key;
    }
}

/* Before you start:  you cannot use the [] operators anywhere
 * in this lab with the exception of declaring the array of integers
 * you must use ptr math and the & * operators to accomplish the tasks
 * below.  Function prototypes should be in the appropriate file.  You
 * may find additional parameters to the functions below to
 * be useful...I don't give you everything you may need.
 *
 * task 0 - create a *.h file to accompany this *.c file and modify it
 * as you progress through the tasks below
 * task 1 - create a main function in this *.c file
 * task 1a - call and test each of the functions as you complete the
 * remaining tasks
 * task 2 - write a function that generates random integer values and
 * assigns them to an array when the address of the array is passed in
 * task 4 - write a function that prints the values in the array when the
 * address of the array is passed to the function
 * task 5 - write a function that adds a given amount to each
 * item in the array when the address of the array is passed to the function
 * task 6 - write insertion sort given the address to the array
 */