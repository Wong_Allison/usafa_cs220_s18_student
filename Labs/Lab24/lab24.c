/** lab24.c
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Header for BST library
* =================================================================
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "bstAsLinkedDS.h"
#define max(x,y) (((x)>(y))?(x):(y))
/* Instructions - Complete the following tasks
 *
 * Task0 - complete the remaining operations on
 * the BST as defined in the header file,
 * bstAsLinkedDS.h (new version for lab 24).
 * Test with the code below.  Feel free to
 * add / change tests below to meet your needs.
 *
 * Note: displayBST() is optional
 *
 */
int main() {
    BSTnode* root = NULL;
    root = insertBST(root,5);
    root = insertBST(root,10);
    root = insertBST(root,1);
    root = insertBST(root,7);
    root = insertBST(root,3);
    root = insertBST(root,6);
    printInorderBST(root);printf("\n");
    printPreorderBST(root);printf("\n");
    printPostorderBST(root);printf("\n");
    BSTnode* curNode = searchBST(root,10);
    if (curNode == NULL) {
        printf("Error - Not found.\n");
    } else {
        printf("found -> %d\n", curNode->data);
    }
    printf("contains? -> %d\n", containsBST(root,3));
    printf("size -> %d\n",sizeBST(root));
    printf("height -> %d\n",heightBST(root));
    root = deleteNodeBST(root,6);
    root = deleteNodeBST(root,3);
    root = deleteNodeBST(root,5);
    destroyBST(root);
}

BSTnode* insertBST(BSTnode* root, int item) {
    BSTnode *newNode = malloc(sizeof(BSTnode));
    newNode->data = item;
    newNode->left = NULL;
    newNode->right = NULL;
    return insertBSTRec(root, newNode);
//    if(root == NULL){
//        return newNode;
//    }
//    else{
//        return insertBSTRec(root, newNode);
//    }
}

BSTnode* insertBSTRec(BSTnode* root, BSTnode* newNode){
    if(root == NULL){
        return newNode;
    }

    if(newNode->data < root->data){
        root->left = insertBSTRec(root->left, newNode);
    }
    else{
        root->right = insertBSTRec(root->right, newNode);
    }
    return root;
}


void printInorderBST(BSTnode* nodePtr){
    if(nodePtr != NULL){
        printInorderBST(nodePtr->left);
        printf("%d ", nodePtr->data);
        printInorderBST(nodePtr->right);
    }
}

void printPreorderBST(BSTnode* nodePtr){
    if(nodePtr != NULL){
        printf("%d ", nodePtr->data);
        printPreorderBST(nodePtr->left);
        printPreorderBST(nodePtr->right);
    }
}

void printPostorderBST(BSTnode* nodePtr) {
    if (nodePtr != NULL) {
        printPostorderBST(nodePtr->left);
        printPostorderBST(nodePtr->right);
        printf("%d ", nodePtr->data);
    }
}

BSTnode* searchBST(BSTnode* node, int item) {
    if((node == NULL)||(node->data == item)){
        return node;
    }
    if(node->data > item){
        return searchBST(node->left, item);
    }
    else{
        return searchBST(node->right, item);
    }
}

bool containsBST(BSTnode* node, int item){
    if(searchBST(node, item) != NULL){
        return true;
    }
    return false;
}

int sizeBST(BSTnode* node){
    if(node == NULL){
        return 0;
    }
    return sizeBST(node->left) + sizeBST(node->right) + 1;
}

int heightBST(BSTnode* node){
    if(node == NULL){
        return 0;
    }
    return max(sizeBST(node->left), sizeBST(node->right)) + 1;
}

void destroyBST(BSTnode* node){

}

BSTnode* deleteNodeBST(BSTnode* node, int item){
    if(node->data == NULL){
        return NULL;
    }
    //if node left = NULL
    if(node->left == NULL){
        BSTnode* tempNode = node->right;
        free(node);
        return tempNode;
    }
    //if node right == NULL
    else if(node->right == NULL) {
        BSTnode *tempNode = node->left;
        free(node);
        return tempNode;
    }
    // two children
    else{
        BSTnode* tempNodeLeft = node->left;;
        while(tempNodeLeft->right != NULL){
            tempNodeLeft = tempNodeLeft->right;
        }
        node = tempNodeLeft;
        return node;
    }
}

