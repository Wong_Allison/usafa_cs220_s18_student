/** listAsLinkedList.h
* ================================================================
* Name: Allison Wong
* Section: T3A
* Project: Header for Linked List Library
*
* Instructions:  Complete the implementation file for this header file
* (For lab 16 on lesson 16 only complete part 1).  Also create a menu
* system like lab 13 to test your listAsLinkedList library
*
*
*
* =================================================================
*/
#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#include <stdbool.h>

// Define a node of the linked list
typedef struct node {
    int data;
    struct node* next;
} Node;

// Define the type (meta-data) that manages the linked list of nodes
typedef struct {
    Node* head;
    Node* tail;
    int numberOfElements;
} LinkedList;

// Functions that manipulate a linked list
// lab 16 part 1
LinkedList* createLinkedList();
void deleteLinkedList(LinkedList* list);
void appendElementLinkedList(LinkedList* list, int element);
int lengthOfLinkedList(LinkedList* list);
void printLinkedList(LinkedList* list);
int getElementLinkedList(LinkedList* list, int position);
void changeElementLinkedList(LinkedList* list, int position, int newElement);

LinkedList* createdLinkedList(){
    LinkedList *list = malloc(sizeof(LinkedList));
}

void deleteLinkedList(LinkedList* list) {
    Node *tempPtr = list->head;
    Node *freePtr = list->head;
    while (freePtr != NULL) {
        tempPtr = tempPtr->next;
        free(freePtr);
        freePtr = tempPtr;
    }
    list->head = NULL;
    list->tail = NULL;
    list->numberOfElements = 0;
}

void appendElementLinkedList(LinkedList* list, int element){
    Node* tempNode = malloc(sizeof(Node));
    tempNode->data = element;
    tempNode->next = NULL;
    list->tail->next = tempNode;
    list->tail = tempNode;
    list->numberOfElements++;
}

int lengthOfLinkedList(LinkedList* list){
    int count = 0;
    struct Node* tempList = list->head;
    while(tempList != NULL){
        count++;
        tempList = tempList->next;
    }
    return count;
}

void printLinkedList(LinkedList* list){
    Node* tempPtr = list->head;
    while(tempPtr != NULL) {
        printf("%d\n", tempPtr->data);
        tempPtr = tempPtr->next;
    }
}

int getElementLinkedList(LinkedList* list, int position){
    Node* tempPtr = list->head;
    for(int i = 0; i < position; ++i){
        tempPtr = tempPtr->next;
    }
    return tempPtr->data;
}

void changeElementLinkedList(LinkedList* list, int position, int newElement){
    int i = 0;
    Node* overwrite = list->head;
    while(i < position){
        i++;
        overwrite = overwrite->next;
    }
    overwrite->data = newElement;
}


// lab 16 part 2
void deleteElementLinkedList(LinkedList* list, int position);
void insertElementLinkedList(LinkedList* list, int position, int element);
int findElementLinkedList(LinkedList* list, int element);
void insertSortLinkedList(LinkedList* list);

#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
